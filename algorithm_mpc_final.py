from mpyc.runtime import mpc    # load MPyC
from mpyc.seclists import seclist
secint = mpc.SecInt()           # 32-bit secure MPyC integers
secflt = mpc.SecFlt()
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages
from oblif.decorator import oblif




def get_intersection(liste_croissante, liste_decroissante):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)
    i = 0
    switched = False
    to_divide = False
    
    #toutes les conditions vont à chaque fois etre exécutée mais seulement la vrai réponse sera gardée, d'ou les vérifications de i

    def condition_0():
        mpc.run(mpc.output(secint(-10)))
        print("cond 0 exécutée")
        #print("pas de solutions")
        nonlocal switched
        switched = True
        return -100
        
    def condition_1():
        print("cond 1 exécutée")
        mpc.run(mpc.output(secint(0)))
        nonlocal i
        mpc.run(mpc.output(secint(i)))
        #print("solution (les deux échelons sont constants au meme niveau) ", i)
        nonlocal switched
        switched = True
        return i
        
    def condition_2():
        print("cond 2 exécutée")
        mpc.run(mpc.output(secint(-10)))
        #print("on saute, ya R")
        nonlocal i
        i = i+1
        return 0
        
    def condition_3():
        print("cond 3 exécutée")
        nonlocal switched
        switched = True
        mpc.run(mpc.output(secint(-10)))
        #print("pas de solutions")
        return -100
        
    def condition_4():
        print("cond 4 exécutée")
        nonlocal switched
        switched = True
        nonlocal i
        if((i-1) == max):
            return 0
        
        #mpc.convert((liste_croissante[i-1] + liste_decroissante[i-1])/2, secflt)

        #a = secflt(liste_croissante[i-1])
        #mpc.convert(secint(i), secflt)

        #mpc.to_secure()
        #print("ONDZOJNFEOZJNEF ", mpc.run(mpc.output((liste_croissante[i-1] + liste_decroissante[i-1])/2)))
        #a = (liste_croissante[i-1] + liste_decroissante[i-1])/2
        #print("solution : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
        #Normalement c'est (liste_croissante[i-1] + liste_decroissante[i-1])/2 mais on va éviter de faire la division en MPC
        nonlocal to_divide
        to_divide = True
        res = mpc.add(liste_croissante[i-1], liste_decroissante[i-1])
        print("mpc add : ", mpc.run(mpc.output(res)))
        return (res)
        
    def condition_5():
        print("cond 5 exécutée")
        nonlocal switched
        nonlocal i
        switched = True
        if(i == max):
            return 0
        mpc.run(mpc.output(liste_croissante[i]))
        #print("i = dsdsdsd ", i)
        #print("solution : ", liste_croissante[i])
        return liste_croissante[i]
        
    def condition_6():
        print("cond 6 exécutée")
        nonlocal switched
        switched = True
        nonlocal i
        if(i == max):
            return 0
        mpc.run(mpc.output(liste_decroissante[i]))
        #print("solution : ", liste_decroissante[i])
        return liste_decroissante[i]

    result = -50


    while(not switched):

        #initialize all the next (ALL) variables by 
        ret_if_1 = -2
        ret_if_2 = -2
        ret_if_3 = -2
        ret_if_0 = -2
        ret_2 = -2
        ret_1 = -2

        if(i==max):
            break

        #ret_if_3_and = mpc.and_((liste_croissante[i] != liste_croissante[i-1]), (liste_decroissante[i] == liste_decroissante[i-1]))
       
        ret_if_3_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_decroissante[i], -4)

        ret_if_3 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]), ret_if_3_, -4)
        #ret_if_3 = ((liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1])).if_else(liste_decroissante[i], -4)
       
        #(liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])
        ret_if_2_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_croissante[i], ret_if_3)
        
        
        ret_if_2 = mpc.if_else((liste_croissante[i] == liste_croissante[i-1]), ret_if_2_, ret_if_3)
        #ret_if_2 = ((liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])).if_else(liste_croissante[i], ret_if_3)
        
        
        
        
        #Normalement c'est (liste_croissante[i-1] + liste_decroissante[i-1])/2 mais on va éviter de faire la division en MPC
        ret_if_1_ = mpc.if_else((liste_decroissante[i] != liste_decroissante[i-1]), (liste_croissante[i-1] + liste_decroissante[i-1]), ret_if_2)
        #(liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])
        
        
        ret_if_1 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]) , ret_if_1_, ret_if_2)
        #ret_if_1 = ((liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])).if_else((liste_croissante[i-1] + liste_decroissante[i-1])/2, ret_if_2)
        
        print("ret_if_1 : ", ret_if_1)
        print("liste_croissante[i] = ", liste_croissante[i])
        print("liste_decroissante[i] = ", liste_decroissante[i])
        print("ret_if_1_ : ", ret_if_1_)
        print("ret_if_2 : ", ret_if_2)

        ret_if_2_0 = mpc.if_else(ret_if_1==ret_if_1_, 1, -1)

        #normalement c'est (liste_croissante[i-1] + liste_decroissante[i-1])/2 mais on peut éviter de faire la division en MPC
        ret_if_2_1 = mpc.if_else(ret_if_1_==(liste_croissante[i-1] + liste_decroissante[i-1]), 1, -1)

        print("liste_croissante[i-1] = ", liste_croissante[i-1])
        print("liste_decroissante[i-1] = ", liste_decroissante[i-1])
        print("liste_croissante[i-1] = ", mpc.run(mpc.output(liste_croissante[i-1])))
        print("liste_decroissante[i-1] = ", mpc.run(mpc.output(liste_decroissante[i-1])))
        #mpc.np_divide(liste_croissante[i-1] + liste_decroissante[i-1], secint(2)) ##!!!!!!!!!!!!!
        print("i = ", i)
        #print("liste_croissante[i-1]*1/2 :", mpc.prod(liste_croissante[i-1], 0.5))
        #print("moyenne = ", mpc.run(mpc.output((liste_croissante[i-1] + liste_decroissante[i-1])/2)))

        ret_if_0 = mpc.if_else(i==0, -1, ret_if_1)
        #ret_if_0 = (i==0).if_else(-1, ret_if_1)
        
        print("POURTANT :")
        print("liste_decroissante[i] = ", mpc.run(mpc.output(liste_decroissante[i])))
        print("liste_croissante[i] = ", mpc.run(mpc.output(liste_croissante[i])))
        ret_2 = mpc.if_else(liste_decroissante[i] < liste_croissante[i], ret_if_0, -5) #-5 <=> i+=1
        #ret_2 = (liste_decroissante[i] < liste_croissante[i]).if_else(ret_if_0, i+1)
        
        ret_1 = mpc.if_else(liste_decroissante[i] == liste_croissante[i], i, ret_2)
        #ret_1 = (liste_decroissante[i] == liste_croissante[i]).if_else(i, ret_2)

        ######################

        ret_0 = mpc.if_else(i == max, -1, ret_1)
        #ret_0 = (i == max).if_else(-1, ret_1)

        ret__0 = mpc.if_else(ret_0 == -1, -1, 1)
        #ret_0 = (ret_0 == -1).if_else(-1, 1)

        ret__1 = mpc.if_else(i == max, -1, 1)
        #ret_1 = (ret_if_0 == -1).if_else(-1, 1)


        ret_if_i_max = mpc.if_else(i == max, 1, -1)
        ret_if_croissante_e_decroissante = mpc.if_else(ret_2 == -2, 1, -1)
        ret_if_decroissante_inf_decroissante = mpc.if_else(ret_2 == -5, -1, 1)
        ret_if_e_zero = mpc.if_else(ret_if_1 == -2, 1, -1)

        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_2==-2, 1, -1)
        print("ret_if_2_0 = ",ret_if_2_0)
        print("ret_if_2_1 = ", ret_if_2_1)
        
        ############

        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1_0 = mpc.if_else(ret_if_2_1==1, 1, -1)
        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1_1 = mpc.if_else(ret_if_2_0==1, 1, -1)

        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1_0 + ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1_1 == 1, 1, -1)

        ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else((ret_if_2_0 + ret_if_2_1) ==2, 1, -1)

        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_2_0==1 and ret_if_2_1==1, 1, -1)

        ############
        ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_3==-4, 1, -1)
        ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0 = mpc.if_else(ret_if_3_==-4, 1, -1)
        ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1 = mpc.if_else(ret_if_3==-4, 1, -1)

        #print all this values
        print("ret_if_i_max : ", ret_if_i_max)
        print("ret_if_croissante_e_decroissante : ", mpc.run(mpc.output(ret_if_croissante_e_decroissante)))
        print("ret_if_decroissante_inf_decroissante : ", mpc.run(mpc.output(ret_if_decroissante_inf_decroissante)))
        print("ret_if_e_zero : ", mpc.run(mpc.output(ret_if_e_zero)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1)))
        print("ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1 : ", mpc.run(mpc.output(ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1)))


        #On va changer l'algo suivant de sorte à ce qu'on affiche de l'information ssi c'est la dernière itération et donc les résultats finaux

        final_7 = mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1==-1, -12, -2)
        final_6 = mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0==-1, final_7, -2)
        final_5 = mpc.if_else(ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1==1, -11, final_6)
        final_4 = mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1==1, -10, final_5)##
        final_3 = mpc.if_else(ret_if_e_zero==1, -9, final_4)
        final_2 = mpc.if_else(ret_if_decroissante_inf_decroissante==-1, -8, final_3)
        final_1 = mpc.if_else(ret_if_croissante_e_decroissante==1, -7, final_2)
        final_0 = mpc.if_else(ret_if_i_max==1, -6, final_1)

        result = mpc.if_else(final_0==-6, condition_0(), 
                    mpc.if_else(final_1==-7, condition_1(),
                                mpc.if_else(final_2==-8, condition_2(),
                                            mpc.if_else(final_3==-9, condition_3(),
                                                        mpc.if_else(final_4==-10, condition_4(), 
                                                                    mpc.if_else(final_5==-11, condition_5(),
                                                                                mpc.if_else(final_6==-2, -2, 
                                                                                            mpc.if_else(final_7==-2, -2, condition_6()))))))))

        



        #final_0 = mpc.if_else(ret_if_i_max==1, func(2), final_1)


        #mpc.if_else(6 == -6, lambda: print(mpc.run(mpc.output(secint(4)))), -6)


        #break
        
        """

        if(ret_if_i_max==1):
            print("pas de solutions")
            break
        elif ret_if_croissante_e_decroissante==1:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
            break
        elif ret_if_decroissante_inf_decroissante==-1:
            print("on saute, ya R")
            i = i+1
        elif ret_if_e_zero==1:
            switched = True
            print("pas de solutions")
        elif ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1==1:
            switched = True
            print("solution : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
        elif ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1==1:
            switched = True
            print("solution : ", liste_croissante[i])
        elif ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0==-1 and ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1==-1:
            switched = True
            print("solution : ", liste_decroissante[i])
        else:
            print("wtf", i)

        print("I = : ", i)

        print(type(ret__1))
        print(ret__1)

        print(type(ret__0))
        print(ret__0)
        """"""

        #break

        """

        """
        print('Maximum age:', mpc.output(ret__0))

        if (ret__0==-1):
            print("pas de solutions")
        elif (ret__1==-1):
            print("pas de solutions")
        else:
            switched = True
            print("solution : ", ret_0)
        """

        """
        #######

        #-1 <=> pas de solutions
        #-4 <=> None
        #None
        """
        """

        if i == max:
            print("pas de solutions !")
            break
        elif liste_decroissante[i] == liste_croissante[i]:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
            break
        elif liste_decroissante[i] < liste_croissante[i]:
            #il y a eu une intersection
            switched = True
            #cas 1 : les deux courbes se sont coupées en meme temps
            if i==0:
                print("pas de solutions")
                break
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
            #cas 2 : la courbe de l'offre à coupée celle de la demande
            elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
            #cas 3 : la courbe de la demande à coupée celle de l'offre
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
                print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
            break
        else:
            i+=1  
        

"""

    #le résultat doit etre divisé par 2
    return (result, to_divide)


liste_croissante_0 = [1, 1, 3, 3, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
"""


print('les deux courbes se sont coupées en meme temps')

liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
liste_croissante_0 = [1, 1, 3, 4, 4, 4, 5]

get_intersection(liste_croissante_0, liste_decroissante_0)


print("la courbe de la demande à coupée celle de l'offre")

liste_croissante_0 = [1, 1, 2, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 3, 3, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)


print("la courbe de l'offre à coupée celle de la demande")

liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
liste_croissante_0 = [1, 1, 3, 3, 4, 4, 5]

get_intersection(liste_croissante_0, liste_decroissante_0)


"""
liste_croissante_0 = [1, 1, 2, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 3, 3, 2, 2, 1]

""""""
#get_intersection(liste_croissante_0, liste_decroissante_0)

liste_croissante_0 = [1, 1, 3, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]

#get_intersection(liste_croissante_0, liste_decroissante_0)

liste_decroissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))
liste_croissante = list(map(secint, [1, 1, 3, 4, 4, 4, 5]))

(result, to_divide) = get_intersection(liste_croissante, liste_decroissante)
if to_divide:
    print("résultat (à diviser par 2) : ", mpc.run(mpc.output(result)))
else:
    print("résultat : ", mpc.run(mpc.output(result)))



################
"""
print('les deux courbes se sont coupées en meme temps')

liste_croissante = list(map(secint, [1, 1, 3, 4, 4, 4, 5]))
liste_decroissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))

result = get_intersection(liste_croissante, liste_decroissante)
print("résultat : ", mpc.run(mpc.output(result)), "/2")


print("la courbe de la demande à coupée celle de l'offre")

liste_croissante = list(map(secint, [1, 1, 2, 4, 4, 4, 5]))
liste_decroissante = list(map(secint, [5, 4, 3, 3, 2, 2, 1]))

result = get_intersection(liste_croissante, liste_decroissante)
print("résultat : ", mpc.run(mpc.output(result)), "/2")


print("la courbe de l'offre à coupée celle de la demande")

liste_croissante = list(map(secint, [1, 1, 3, 3, 4, 4, 5]))
liste_decroissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))

result = get_intersection(liste_croissante, liste_decroissante)
print("résultat : ", mpc.run(mpc.output(result)), "/2")

"""

"""

liste_croissante = list(map(secint, [1, 1, 3, 3, 4, 4, 5]))
liste_croissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))

get_intersection(liste_croissante, liste_croissante)
"""
mpc.run(mpc.shutdown())