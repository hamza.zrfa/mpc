from mpyc.runtime import mpc    # load MPyC
secint = mpc.SecInt()           # 32-bit secure MPyC integers
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages

#####
#print(''.join(mpc.run(mpc.transfer('PC Hamza 1'))))

#lst = mpc.run(mpc.transfer([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))

def bitonic_sort(up, x):
    if len(x) <= 1:
        return x
    else: 
        first = bitonic_sort(True, x[:len(x) // 2])
        second = bitonic_sort(False, x[len(x) // 2:])
        return bitonic_merge(up, first + second)

def bitonic_merge(up, x): 
    # assume input x is bitonic, and sorted list is returned 
    if len(x) == 1:
        return x
    else:
        bitonic_compare(up, x)
        first = bitonic_merge(up, x[:len(x) // 2])
        second = bitonic_merge(up, x[len(x) // 2:])
        return first + second

def bitonic_compare(up, x):
    dist = len(x) // 2
    up = secint(up)                                    # convert public Boolean up into `secint` bit 
    for i in range(dist):
        b = (x[i] > x[i + dist]) ^ ~up                 # secure xor of comparison bit and negated up
        d = b * (x[i + dist] - x[i])                   # d = 0 or d = x[i + dist] - x[i]
        x[i], x[i + dist] = x[i] + d, x[i + dist] - d  # secure swap

#####

def bitonic_sort_(up, x, start_index=0):
    if len(x) <= 1:
        return x, start_index
    else: 
        first, first_index = bitonic_sort_(True, x[:len(x) // 2], start_index)
        second, second_index = bitonic_sort_(False, x[len(x) // 2:], start_index + len(first))
        merged, merged_index = bitonic_merge_(up, first + second, start_index)
        return merged, merged_index

def bitonic_merge_(up, x, start_index=0): 
    if len(x) == 1:
        return x, start_index
    else:
        bitonic_compare_(up, x)
        first, first_index = bitonic_merge_(up, x[:len(x) // 2], start_index)
        second, second_index = bitonic_merge_(up, x[len(x) // 2:], start_index + len(first))
        merged = first + second
        return merged, first_index

def bitonic_compare_(up, x):
    dist = len(x) // 2
    up = secint(up)  # convert public Boolean up into `secint` bit 
    for i in range(dist):
        b = (x[i] > x[i + dist]) ^ ~up  # secure xor of comparison bit and negated up
        d = b * (x[i + dist] - x[i])   # d = 0 or d = x[i + dist] - x[i]
        x[i], x[i + dist] = x[i] + d, x[i + dist] - d  # secure swap


#####

#Let the participant to enter the int number
number = int(input('Enter your integer: '))  # each party enters a number
list_ = mpc.input(secint(number))  # list with one secint per party


x = list(map(secint, [2, 4, 3, 5, 6, 1, 7, 8]))
print(mpc.run(mpc.output(bitonic_sort_(True, x))))

#####
mpc.run(mpc.shutdown())