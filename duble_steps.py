import numpy as np
import matplotlib.pyplot as plt

def fonction_echelon(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [1, 3, 4, 5, 9])

def fonction_decroissante(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [9, 5, 4, 3, 1])

# Génération des valeurs x
x = np.linspace(0, 10, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Calcul des fonctions échelon correspondantes
y1 = fonction_echelon(x)
y2 = fonction_decroissante(x)

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Comparaison de fonctions échelon')
plt.grid(True)
plt.legend()
plt.show()
