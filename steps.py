import numpy as np
import matplotlib.pyplot as plt

def fonction_echelon(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
        #(x >= 34) & (x <= 39),
        #(x > 39)
    ], [1, 3, 4, 5, 9])

# Génération des valeurs x
x = np.linspace(0, 10, 10000)  # Crée un tableau de 500 valeurs entre 0 et 100

# Calcul de la fonction échelon correspondante
y = fonction_echelon(x)

# Tracé du graphe
plt.plot(x, y)
plt.xlabel('x')
plt.ylabel('fonction échelon')
plt.title('Graphe de la fonction échelon')
plt.grid(True)
plt.show()
