from mpyc.runtime import mpc    # load MPyC
from mpyc.seclists import seclist
secint = mpc.SecInt()           # 32-bit secure MPyC integers
secflt = mpc.SecFlt()
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages
from oblif.decorator import oblif

#####


def get_intersection(liste_croissante, liste_decroissante):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)
    i = 0
    switched = False
    while(not switched):
        if i == max:
            print("pas de solutions !")
            break
        elif liste_decroissante[i] == liste_croissante[i]:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
            break
        elif liste_decroissante[i] < liste_croissante[i]:
            #il y a eu une intersection
            switched = True
            #cas 1 : les deux courbes se sont coupées en meme temps
            if i==0:
                print("pas de solutions")
                break
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
            #cas 2 : la courbe de l'offre à coupée celle de la demande
            elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
            #cas 3 : la courbe de la demande à coupée celle de l'offre
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
                print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
            break
        else:
            i+=1  


#####

type = None
price = None
quantity = None

#Define the participant type (buyer : 0, seller: 1)
while True:
    choice = int(input("Enter 0 if you are a buyer and 1 otherwise: "))
    if choice == 0:
        type = "buyer"
        print("You are a buyer.")
        break
    elif choice == 1:
        type = "seller"
        print("You are a seller.")
        break
    else:
        print("the input is incorrect")

#Get the price and quantity of the participant
while True:
    price = int(input("Enter the price: "))
    if price > 0:
        break
    else:
        print("Price must be a positive number.")

while True:
    quantity = int(input("Enter the quantity: "))
    if quantity > 0:
        break
    else:
        print("Quantity must be a positive integer.")

# Store the point (price, quantity) in a list
point = seclist([price, quantity], sectype=secint)
#print("point : ")
#print(point)

if type == "buyer":
    list_prices_buyers = mpc.input(secint(price))
    list_quantities_buyers = mpc.input(secint(quantity))
    
    print("list_prices_buyers :")
    print(mpc.run(mpc.output(list_prices_buyers)))

    list_prices_buyers.sort()

    print("list_prices_buyers :")
    print(mpc.run(mpc.output(list_prices_buyers)))


else:
    list_prices_sellers = mpc.input(secint(price))
    list_quantities_sellers = mpc.input(secint(quantity))

    print("list_prices_sellers :")
    print(mpc.run(mpc.output(list_prices_sellers)))

    list_prices_sellers.sort()

    print("list_prices_sellers :")
    print(mpc.run(mpc.output(list_prices_sellers)))


liste_croissante = list(map(secint, [1, 1, 3, 3, 4, 4, 5]))
liste_croissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))

get_intersection(liste_croissante, liste_croissante)

mpc.run(mpc.shutdown())