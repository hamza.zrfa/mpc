#Classic computation

def get_intersection(liste_croissante, liste_decroissante, i):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)

    if i == max:
        #print("pas de solutions !")
        return 0
    elif liste_decroissante[i] == liste_croissante[i]:
        #print("solution (les deux échelons sont constants au meme niveau) ", i)
        return 1
    elif liste_decroissante[i] < liste_croissante[i]:
        #il y a eu une intersection
        switched = True
        #cas 1 : les deux courbes se sont coupées en meme temps
        if i==0:
            #print("pas de solutions")
            return 3
        elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
            #print("solution (les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
            return 4
        #cas 2 : la courbe de l'offre à coupée celle de la demande
        elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
            #print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
            return 5
        #cas 3 : la courbe de la demande à coupée celle de l'offre
        elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
            #print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
            return 6
    else:
        return 2

liste_croissante = [1, 1, 3, 3, 4, 4, 5]
liste_decroissante = [5, 4, 4, 2, 2, 2, 1]

i = 0
stop = 0

while(stop!=1):
    result = get_intersection(liste_croissante, liste_decroissante, i)

    if result==0:
        print("pas de solutions")
        stop = 1
    elif result==1:
        print("solution (les deux échelons sont constants au meme niveau) :", liste_croissante(i))
        stop = 1
    elif result==2:
        i = i+1
    elif result==3:
        print("pas de solutions")
        stop = 1
    elif result==4:
        print("solution (les deux courbes se sont coupées en meme temps): ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
        stop = 1
    elif result==5:
        print("Solution (la courbe de l'offre à coupée celle de la demande): ", liste_croissante[i])
        stop = 1
    elif result==6:
        print("Solution : (la courbe de la demande à coupée celle de l'offre): ", liste_decroissante[i])
        stop = 1

#MP-computation
from mpyc.runtime import mpc    # load MPyC
from mpyc.seclists import seclist
secint = mpc.SecInt()           # 32-bit secure MPyC integers
secflt = mpc.SecFlt()
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages

def get_intersection(liste_croissante, liste_decroissante, i):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)

    ret_if_1 = -2
    ret_if_2 = -2
    ret_if_3 = -2
    ret_if_0 = -2
    ret_2 = -2
    ret_1 = -2

    if i==max:
        return (secint(-55), -1)
        
    ret_if_3_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_decroissante[i], -4)

    ret_if_3 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]), ret_if_3_, -4)
    
    ret_if_2_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_croissante[i], ret_if_3)
        
        
    ret_if_2 = mpc.if_else((liste_croissante[i] == liste_croissante[i-1]), ret_if_2_, ret_if_3)
    
    ret_if_1_ = mpc.if_else((liste_decroissante[i] != liste_decroissante[i-1]), (liste_croissante[i-1] + liste_decroissante[i-1])/2, ret_if_2)
    
    ret_if_1 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]) , ret_if_1_, ret_if_2)
    
    ret_if_2_0 = mpc.if_else(ret_if_1==ret_if_1_, 1, -1)
    ret_if_2_1 = mpc.if_else(ret_if_1_==(liste_croissante[i-1] + liste_decroissante[i-1])/2, 1, -1)
    
    ret_if_0 = mpc.if_else(i==0, -1, ret_if_1)
    
    ret_2 = mpc.if_else(liste_decroissante[i] < liste_croissante[i], ret_if_0, -5) #-5 <=> i+=1
    
    ret_1 = mpc.if_else(liste_decroissante[i] == liste_croissante[i], i, ret_2)
    
    ret_0 = mpc.if_else(i == max, -1, ret_1)
    
    ret__0 = mpc.if_else(ret_0 == -1, -1, 1)
    
    ret__1 = mpc.if_else(i == max, -1, 1)
    
    ret_if_i_max = mpc.if_else(i == max, 1, -1)
    ret_if_croissante_e_decroissante = mpc.if_else(ret_2 == -2, 1, -1)
    ret_if_decroissante_inf_decroissante = mpc.if_else(ret_2 == -5, -1, 1)
    ret_if_e_zero = mpc.if_else(ret_if_1 == -2, 1, -1)

    a = mpc.if_else(ret_if_2_0==1, 1, 0)
    b = mpc.if_else(ret_if_2_1==1, 1, 0)

    #c = a + b # good ?
    c = mpc.add(a,b)
    ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(c==2, 1, -1)
    
    ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_3==-4, 1, -1)
    ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0 = mpc.if_else(ret_if_3_==-4, 1, -1)
    ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1 = mpc.if_else(ret_if_3==-4, 1, -1)

    result = mpc.if_else(ret_if_i_max==1, 0, 
                mpc.if_else(ret_if_croissante_e_decroissante==1, 1,
                            mpc.if_else(ret_if_decroissante_inf_decroissante==-1, 2,
                                        mpc.if_else(ret_if_e_zero==1, 3,
                                                    mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1==1, 4, 
                                                                mpc.if_else(ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1==1, 5,
                                                                            mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0==-1,
                                                                                        mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1==-1, 6, -100), -100)))))))

    final_result = result

    return final_result


liste_decroissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))
liste_croissante = list(map(secint, [1, 1, 3, 3, 4, 4, 5]))

i = 0
stop = 0

while(stop!=1):
    result = mpc.run(mpc.output(get_intersection(liste_croissante, liste_decroissante, i)))

    if result==0:
        print("pas de solutions")
        stop = 1
    elif result==1:
        print("solution (les deux échelons sont constants au meme niveau) :", mpc.run(mpc.output(liste_croissante(i))))
        stop = 1
    elif result==2:
        i = i+1
    elif result==3:
        print("pas de solutions")
        stop = 1
    elif result==4:
        print("solution (les deux courbes se sont coupées en meme temps): ", mpc.run(mpc.output((liste_croissante[i-1] + liste_decroissante[i-1])/2)))
        stop = 1
    elif result==5:
        print("Solution (la courbe de l'offre à coupée celle de la demande): ", mpc.run(mpc.output(liste_croissante[i])))
        stop = 1
    elif result==6:
        print("Solution : (la courbe de la demande à coupée celle de l'offre): ", mpc.run(mpc.output(liste_decroissante[i])))
        stop = 1

mpc.run(mpc.shutdown())