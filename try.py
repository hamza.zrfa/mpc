import matplotlib.pyplot as plt
import numpy as np

def step_function(x):
    return np.floor(x) + 1

# Générer des valeurs pour l'axe x
x = np.linspace(-10, 10, 1000)

# Calculer les valeurs de la fonction étape pour chaque x
y = step_function(x)

# Tracer la fonction étape
plt.step(x, y, where='post')

# Paramètres d'affichage du graphique
plt.xlabel('x')
plt.ylabel('y')
plt.title('Fonction étape croissante')
plt.ylim(0, np.max(y) + 1)  # Plage des valeurs de l'axe y
plt.xlim(0, np.max(x))  # Plage des valeurs de l'axe x pour la partie positive
plt.axvline(0, color='black', linestyle='--')  # Ligne verticale pour marquer zéro
plt.grid(True)

# Afficher le graphique
plt.show()
