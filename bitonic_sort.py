#####

def bitonic_sort(up, x):
    if len(x) <= 1:
        return x
    else: 
        first = bitonic_sort(True, x[:len(x) // 2])
        second = bitonic_sort(False, x[len(x) // 2:])
        return bitonic_merge(up, first + second)

def bitonic_merge(up, x): 
    # assume input x is bitonic, and sorted list is returned 
    if len(x) == 1:
        return x
    else:
        bitonic_compare(up, x)
        first = bitonic_merge(up, x[:len(x) // 2])
        second = bitonic_merge(up, x[len(x) // 2:])
        return first + second

def bitonic_compare(up, x):
    dist = len(x) // 2
    up = secint(up)                                    # convert public Boolean up into `secint` bit 
    for i in range(dist):
        b = (x[i] > x[i + dist]) ^ ~up                 # secure xor of comparison bit and negated up
        d = b * (x[i + dist] - x[i])                   # d = 0 or d = x[i + dist] - x[i]
        x[i], x[i + dist] = x[i] + d, x[i + dist] - d  # secure swap

#####

def tri_tableau(tableau, croissant=True):
    indices = list(range(len(tableau)))  # Indices initiaux
    indices.sort(key=lambda i: tableau[i], reverse=not croissant)  # Tri des indices selon les valeurs du tableau
    tableau_trie = [tableau[i] for i in indices]  # Tableau trié selon les nouveaux indices

    return tableau_trie, indices
