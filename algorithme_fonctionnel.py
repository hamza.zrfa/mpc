
def get_intersection(liste_croissante, liste_decroissante):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)
    i = 0
    switched = False
    while(not switched):
        if i == max:
            print("pas de solutions !")
            break
        elif liste_decroissante[i] == liste_croissante[i]:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
            break
        elif liste_decroissante[i] < liste_croissante[i]:
            #il y a eu une intersection
            switched = True
            #cas 1 : les deux courbes se sont coupées en meme temps
            if i==0:
                print("pas de solutions")
                break
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
            #cas 2 : la courbe de l'offre à coupée celle de la demande
            elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
            #cas 3 : la courbe de la demande à coupée celle de l'offre
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
                print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
            break
        else:
            i+=1

liste_croissante_0 = [1, 1, 3, 3, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)

liste_croissante_0 = [1, 1, 2, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 3, 3, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)

liste_croissante_0 = [1, 1, 3, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)

"""
i = 0
while(not switched):


    if i == max:
        print("pas de solutions !")
        break
    elif liste_decroissante[i] == liste_croissante[i]:
        print("solution ! ", i)
    elif liste_decroissante[i] < liste_croissante[i]:
        #il y a eu une intersection
        switched = True
        #cas 1 : les deux courbes se sont coupées en meme temps
        #fais gaffe au cas i = 0
        if (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
            print("solution 'les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i] + liste_decroissante[i])/2)
        #cas 2 : la courbe de l'offre à coupée celle de la demande
        elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
            print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
        #cas 3 : la courbe de la demande à coupée celle de l'offre
        elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
            print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
    else:
        i+=1  
"""


