from mpyc.runtime import mpc    # load MPyC
from mpyc.seclists import seclist
secint = mpc.SecInt()           # 32-bit secure MPyC integers
secflt = mpc.SecFlt()
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages
from oblif.decorator import oblif

a = secint(5)
b= secint(5)

c=4

mpc.if_else(a==b, c=9, 4)

print(c)

mpc.run(mpc.shutdown())