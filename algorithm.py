import numpy as np
import matplotlib.pyplot as plt

def fonction_croissante_0(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [1, 1, 3, 3, 4, 4, 5])

def fonction_decroissante_0(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [5, 4, 4, 2, 2, 2, 1])

# Génération des valeurs x
x = np.linspace(0, 7, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Calcul des fonctions échelon correspondantes
y1 = fonction_croissante_0(x)
y2 = fonction_decroissante_0(x)

# Recherche de l'intersection
intersection_indices = np.where(y1 == y2)[0]
if len(intersection_indices) > 0:
    intersection_x = x[intersection_indices[0]]
    intersection_y = y1[intersection_indices[0]]
    #print(f"L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    a = 1
    #print("Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 1 : la courbe de l\'offre coupe celle de la demande')
plt.grid(True)
plt.legend()
plt.show()

#############################

def fonction_croissante_1(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [1, 1, 2, 4, 4, 4, 5])

def fonction_decroissante_1(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [5, 4, 3, 3, 2, 2, 1])

# Génération des valeurs x
x = np.linspace(0, 7, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Calcul des fonctions échelon correspondantes
y1 = fonction_croissante_1(x)
y2 = fonction_decroissante_1(x)

# Recherche de l'intersection
intersection_indices = np.where(y1 == y2)[0]
if len(intersection_indices) > 0:
    intersection_x = x[intersection_indices[0]]
    intersection_y = y1[intersection_indices[0]]
    #print(f"L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    a = 1
    #print("Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 2 : la courbe de la demande coupe celle de l\'offre')
plt.grid(True)
plt.legend()
plt.show()

#############################

def fonction_croissante_2(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [1, 1, 3, 4, 4, 4, 5])

def fonction_decroissante_2(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 1),
        (x >= 1) & (x < 2),
        (x >= 2) & (x < 3),
        (x >= 3) & (x < 4),
        (x >= 4) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x <= 7)
    ], [5, 4, 4, 2, 2, 2, 1])

# Génération des valeurs x
x = np.linspace(0, 7, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Calcul des fonctions échelon correspondantes
y1 = fonction_croissante_2(x)
y2 = fonction_decroissante_2(x)

# Recherche de l'intersection
intersection_indices = np.where(y1 == y2)[0]
if len(intersection_indices) > 0:
    intersection_x = x[intersection_indices[0]]
    intersection_y = y1[intersection_indices[0]]
    #print(f"L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    a = 1
    #print("Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 3 : les deux courbes se coupent en meme temps')
plt.grid(True)
plt.legend()
plt.show()
