import time
import random
import matplotlib.pyplot as plt

# Algorithme de tri bulle (Bubble Sort)
def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]

# Algorithme de tri fusion (Merge Sort)
def merge_sort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        left_half = arr[:mid]
        right_half = arr[mid:]

        merge_sort(left_half)
        merge_sort(right_half)

        i, j, k = 0, 0, 0

        while i < len(left_half) and j < len(right_half):
            if left_half[i] < right_half[j]:
                arr[k] = left_half[i]
                i += 1
            else:
                arr[k] = right_half[j]
                j += 1
            k += 1

        while i < len(left_half):
            arr[k] = left_half[i]
            i += 1
            k += 1

        while j < len(right_half):
            arr[k] = right_half[j]
            j += 1
            k += 1

# Fonction pour générer une liste aléatoire de taille n
def generate_random_list(n):
    return [random.randint(1, 1000) for _ in range(n)]

# Fonction pour mesurer le temps d'exécution d'un algorithme de tri
def measure_time(algorithm, arr):
    start_time = time.time()
    algorithm(arr)
    end_time = time.time()
    return end_time - start_time

# Benchmark complet entre le tri bulle et le tri fusion
def full_benchmark(list_sizes):
    bubble_times = []
    merge_times = []
    for size in list_sizes:
        arr = generate_random_list(size)
        arr_copy = arr.copy()

        # Mesurer le temps d'exécution pour le tri bulle
        bubble_time = measure_time(bubble_sort, arr)
        bubble_times.append(bubble_time)

        # Mesurer le temps d'exécution pour le tri fusion
        merge_time = measure_time(merge_sort, arr_copy)
        merge_times.append(merge_time)

    # Afficher les résultats
    plt.plot(list_sizes, bubble_times, label='Bubble Sort')
    plt.plot(list_sizes, merge_times, label='Merge Sort')
    plt.xlabel('Taille de la liste en entrée')
    plt.ylabel('Temps d\'exécution (secondes)')
    plt.legend()
    plt.title('Comparaison des temps d\'exécution entre Bubble Sort et Merge Sort')
    plt.show()

# Tailles des listes à tester
list_sizes = [100, 500, 1000, 2000, 5000]

# Effectuer le benchmark complet
full_benchmark(list_sizes)
