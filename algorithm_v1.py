import numpy as np
import matplotlib.pyplot as plt

def fonction_croissante_0(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [1, 3, 4, 5, 9])

def fonction_decroissante_0(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [9, 5, 4, 3, 1])

def fonction_croissante_1(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [1, 3, 4, 5, 9])

def fonction_decroissante_1(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [9, 5, 3, 3, 1])

def fonction_croissante_2(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 5),
        (x >= 5) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [1, 3, 5, 5, 9])

def fonction_decroissante_2(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [9, 5, 4, 3, 1])

# Génération des valeurs x
x = np.linspace(0, 10, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Cas 1
y1 = fonction_croissante_0(x)
y2 = fonction_decroissante_0(x)

# Recherche de l'intersection
transition_indices = np.where(np.diff(y1 != y2))[0] + 1
if len(transition_indices) > 0:
    intersection_x = x[transition_indices[0]]
    intersection_y = y1[transition_indices[0]]
    print(f"Cas 1: L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    print("Cas 1: Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 1 : les deux fonctions se coupent en même temps')
plt.grid(True)
plt.legend()
plt.show()

# Cas 2
y1 = fonction_croissante_1(x)
y2 = fonction_decroissante_1(x)

# Recherche de l'intersection
transition_indices = np.where(np.diff(y1 != y2))[0] + 1
if len(transition_indices) > 0:
    intersection_x = x[transition_indices[0]]
    intersection_y = y1[transition_indices[0]]
    print(f"Cas 2: L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    print("Cas 2: Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 2 : la fonction de l\'offre coupe celle de la demande')
plt.grid(True)
plt.legend()
plt.show()

# Cas 3
y1 = fonction_croissante_2(x)
y2 = fonction_decroissante_2(x)

# Recherche de l'intersection
transition_indices = np.where(np.diff(y1 != y2))[0] + 1
if len(transition_indices) > 0:
    intersection_x = x[transition_indices[0]]
    intersection_y = y1[transition_indices[0]]
    print(f"Cas 3: L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    print("Cas 3: Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Cas 3 : la fonction de la demande coupe celle de l\'offre')
plt.grid(True)
plt.legend()
plt.show()
