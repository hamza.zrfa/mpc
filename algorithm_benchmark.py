import numpy as np
import matplotlib.pyplot as plt

def fonction_echelon(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [1, 3, 4, 5, 9])

def fonction_decroissante(x):
    return np.piecewise(x, [
        (x >= 0) & (x < 2),
        (x >= 2) & (x < 4),
        (x >= 4) & (x < 6),
        (x >= 6) & (x < 7),
        (x >= 7) & (x <= 10)
    ], [9, 5, 4, 3, 1])

# Génération des valeurs x
x = np.linspace(0, 10, 10000)  # Crée un tableau de 10000 valeurs entre 0 et 10

# Calcul des fonctions échelon correspondantes
y1 = fonction_echelon(x)
y2 = fonction_decroissante(x)

# Recherche de l'intersection
intersection_indices = np.where(y1 == y2)[0]
if len(intersection_indices) > 0:
    intersection_x = x[intersection_indices[0]]
    intersection_y = y1[intersection_indices[0]]
    print(f"L'intersection des courbes se trouve à x = {intersection_x} avec une ordonnée de y = {intersection_y}")
else:
    print("Les courbes ne se croisent pas.")

# Tracé des graphes
plt.plot(x, y1, label='Fonction échelon', color='blue')
plt.plot(x, y2, label='Fonction décroissante', color='red')
plt.xlabel('x')
plt.ylabel('Valeur')
plt.title('Comparaison de fonctions échelon')
plt.grid(True)
plt.legend()
plt.show()


# --- Tests ---

# Test 1: Intersection entre les fonctions échelon et décroissante
x_test1 = np.linspace(0, 10, 10000)
y1_test1 = np.array([1] * 2000 + [3] * 2000 + [4] * 2000 + [5] * 1000 + [9] * 2000)
y2_test1 = np.array([9] * 2000 + [5] * 2000 + [4] * 2000 + [3] * 1000 + [1] * 2000)

intersection_indices_test1 = np.where(y1_test1 == y2_test1)[0]
if len(intersection_indices_test1) > 0:
    intersection_x_test1 = x_test1[intersection_indices_test1[0]]
    intersection_y_test1 = y1_test1[intersection_indices_test1[0]]
    print(f"Test 1 - Intersection: x = {intersection_x_test1}, y = {intersection_y_test1}")
else:
    print("Test 1 - Les courbes ne se croisent pas.")


# Test 2: Pas d'intersection entre les fonctions échelon et décroissante
x_test2 = np.linspace(0, 10, 10000)
y1_test2 = np.array([1] * 2000 + [3] * 2000 + [4] * 2000 + [5] * 1000 + [9] * 2000)
y2_test2 = np.array([10] * 10000)

intersection_indices_test2 = np.where(y1_test2 == y2_test2)[0]
if len(intersection_indices_test2) > 0:
    intersection_x_test2 = x_test2[intersection_indices_test2[0]]
    intersection_y_test2 = y1_test2[intersection_indices_test2[0]]
    print(f"Test 2 - Intersection: x = {intersection_x_test2}, y = {intersection_y_test2}")
else:
    print("Test 2 - Les courbes ne se croisent pas.")


# Test 3: Plusieurs intersections entre les fonctions échelon et décroissante
x_test3 = np.linspace(0, 10, 10000)
y1_test3 = np.array([1] * 4000 + [3] * 2000 + [4] * 2000 + [5] * 1000 + [9] * 2000)
y2_test3 = np.array([9] * 4000 + [5] * 2000 + [4] * 2000 + [3] * 1000 + [1] * 2000)

intersection_indices_test3 = np.where(y1_test3 == y2_test3)[0]
if len(intersection_indices_test3) > 0:
    for idx in intersection_indices_test3:
        intersection_x_test3 = x_test3[idx]
        intersection_y_test3 = y1_test3[idx]
        print(f"Test 3 - Intersection: x = {intersection_x_test3}, y = {intersection_y_test3}")
else:
    print("Test 3 - Les courbes ne se croisent pas.")
