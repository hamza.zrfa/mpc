from mpyc.runtime import mpc    # load MPyC
from mpyc.seclists import seclist
secint = mpc.SecInt()           # 32-bit secure MPyC integers
secflt = mpc.SecFlt()
mpc.run(mpc.start())            # required only when run with multiple parties
import traceback                # to show some suppressed error messages
from oblif.decorator import oblif



def get_intersection(liste_croissante, liste_decroissante):
    max = len(liste_decroissante) if len(liste_decroissante) < len(liste_croissante) else len(liste_croissante)
    i_ = secint(0)
    i = mpc.run(mpc.output(i_))
    switched = secint(0)
    switched_revealed = mpc.run(mpc.output(switched))

    #mpc.if_else(switched==0)

    

    while(True):

        #initialize all the next (ALL) variables by 
        ret_if_1 = -2
        ret_if_2 = -2
        ret_if_3 = -2
        ret_if_0 = -2
        ret_2 = -2
        ret_1 = -2

        #ret_if_3_and = mpc.and_((liste_croissante[i] != liste_croissante[i-1]), (liste_decroissante[i] == liste_decroissante[i-1]))
       
        if i==max:
            return (secint(-55), -1)
        
        ret_if_3_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_decroissante[i], -4)

        ret_if_3 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]), ret_if_3_, -4)
        #ret_if_3 = ((liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1])).if_else(liste_decroissante[i], -4)
       
        #(liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])
        ret_if_2_ = mpc.if_else((liste_decroissante[i] == liste_decroissante[i-1]), liste_croissante[i], ret_if_3)
        
        
        ret_if_2 = mpc.if_else((liste_croissante[i] == liste_croissante[i-1]), ret_if_2_, ret_if_3)
        #ret_if_2 = ((liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])).if_else(liste_croissante[i], ret_if_3)
        
        
        
        
        ret_if_1_ = mpc.if_else((liste_decroissante[i] != liste_decroissante[i-1]), (liste_croissante[i-1] + liste_decroissante[i-1])/2, ret_if_2)
        #(liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])
        
        
        ret_if_1 = mpc.if_else((liste_croissante[i] != liste_croissante[i-1]) , ret_if_1_, ret_if_2)
        #ret_if_1 = ((liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1])).if_else((liste_croissante[i-1] + liste_decroissante[i-1])/2, ret_if_2)
        
        print("ret_if_1 : ", ret_if_1)
        print("liste_croissante[i] = ", liste_croissante[i])
        print("liste_decroissante[i] = ", liste_decroissante[i])
        print("ret_if_1_ : ", ret_if_1_)
        print("ret_if_2 : ", ret_if_2)

        ret_if_2_0 = mpc.if_else(ret_if_1==ret_if_1_, 1, -1)
        ret_if_2_1 = mpc.if_else(ret_if_1_==(liste_croissante[i-1] + liste_decroissante[i-1])/2, 1, -1)
        print("liste_croissante[i-1] = ", liste_croissante[i-1])
        print("liste_decroissante[i-1] = ", liste_decroissante[i-1])
        print("i = ", i)
        print("moyenne = ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)


        ret_if_0 = mpc.if_else(i==0, -1, ret_if_1)
        #ret_if_0 = (i==0).if_else(-1, ret_if_1)
        
        ret_2 = mpc.if_else(liste_decroissante[i] < liste_croissante[i], ret_if_0, -5) #-5 <=> i+=1
        #ret_2 = (liste_decroissante[i] < liste_croissante[i]).if_else(ret_if_0, i+1)
        
        ret_1 = mpc.if_else(liste_decroissante[i] == liste_croissante[i], i, ret_2)
        #ret_1 = (liste_decroissante[i] == liste_croissante[i]).if_else(i, ret_2)

        ######################

        ret_0 = mpc.if_else(i == max, -1, ret_1)
        #ret_0 = (i == max).if_else(-1, ret_1)

        ret__0 = mpc.if_else(ret_0 == -1, -1, 1)
        #ret_0 = (ret_0 == -1).if_else(-1, 1)

        ret__1 = mpc.if_else(i == max, -1, 1)
        #ret_1 = (ret_if_0 == -1).if_else(-1, 1)


        ret_if_i_max = mpc.if_else(i == max, 1, -1)
        ret_if_croissante_e_decroissante = mpc.if_else(ret_2 == -2, 1, -1)
        ret_if_decroissante_inf_decroissante = mpc.if_else(ret_2 == -5, -1, 1)
        ret_if_e_zero = mpc.if_else(ret_if_1 == -2, 1, -1)

        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_2==-2, 1, -1)
        print("ret_if_2_0 = ", mpc.run(mpc.output(ret_if_2_0)))
        print("ret_if_2_1 = ", mpc.run(mpc.output(ret_if_2_1)))

        ###
        #original ->
        #ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_2_0==1 and ret_if_2_1==1, 1, -1)

        a = mpc.if_else(ret_if_2_0==1, 1, 0)
        b = mpc.if_else(ret_if_2_1==1, 1, 0)

        c = a + b # good ?
        ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(c==2, 1, -1)
        #mpc.and_(ret_if_2_0, ret_if_2_1)
        #nouveau ->
        
        ###

        ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1 = mpc.if_else(ret_if_3==-4, 1, -1)
        ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0 = mpc.if_else(ret_if_3_==-4, 1, -1)
        ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1 = mpc.if_else(ret_if_3==-4, 1, -1)

        #print all this values
        print("ret_if_i_max : ", ret_if_i_max)
        print("ret_if_croissante_e_decroissante : ", mpc.run(mpc.output(ret_if_croissante_e_decroissante)))
        print("ret_if_decroissante_inf_decroissante : ", mpc.run(mpc.output(ret_if_decroissante_inf_decroissante)))
        print("ret_if_e_zero : ", mpc.run(mpc.output(ret_if_e_zero)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1)))
        print("ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1 : ", mpc.run(mpc.output(ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0)))
        print("ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1 : ", mpc.run(mpc.output(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1)))

        """

        if(ret_if_i_max==1):
            print("pas de solutions")
        elif ret_if_croissante_e_decroissante==1:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
        elif ret_if_decroissante_inf_decroissante==-1:
            print("on saute, ya R")
            i = i+1
        elif ret_if_e_zero==1:
            switched = True
            print("pas de solutions")
        elif ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1==1:
            switched = True
            print("solution : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
        elif ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1==1:
            switched = True
            print("solution : ", liste_croissante[i])
        elif ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0==-1 and ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1==-1:
            switched = True
            print("solution : ", liste_decroissante[i])
        else:
            print("wtf", i)
    
        """


        def condition_0():
            nonlocal switched
            switched = secint(0)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            return -100
    
        def condition_1(i):
            nonlocal switched
            switched = secint(0)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            return i
    
        def condition_2():
            nonlocal i
            nonlocal i_
            i_ +=1
            i = mpc.run(mpc.output(i_))
            nonlocal switched
            switched = secint(0)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            print("pourtant frérot avant le return 23, switched vaut: ", mpc.run(mpc.output(switched)))
            return 23
    
        def condition_3():
            nonlocal switched
            switched = secint(1)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            return 15

        def condition_4(sum):
            nonlocal switched
            switched = secint(1)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            #switched = True
            return sum
        
        def condition_5(croissante_sum_0_index):
            nonlocal switched
            switched = secint(1)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            #switched = True
            if croissante_sum_0_index==max:
                return 0
            return liste_croissante[croissante_sum_0_index]
        
        def condition_6(decroissante_sum_1_index):
            nonlocal switched
            switched = secint(1)
            nonlocal switched_revealed
            switched_revealed = mpc.run(mpc.output(switched))
            #switched = True
            if decroissante_sum_1_index==max:
                return 0
            return liste_decroissante[decroissante_sum_1_index]
        

        result = mpc.if_else(ret_if_i_max==1, condition_0(), 
                    mpc.if_else(ret_if_croissante_e_decroissante==1, condition_1(i),
                                mpc.if_else(ret_if_decroissante_inf_decroissante==-1, condition_2(),
                                            mpc.if_else(ret_if_e_zero==1, condition_3(),
                                                        mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_diff_decroi_i_1==1, condition_4((liste_croissante[i-1] + liste_decroissante[i-1])), 
                                                                    mpc.if_else(ret_if_croi_i_e_croi_i__1_decroi_i_diff_decroi_i_1==1, condition_5(i),
                                                                                mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_0==-1,
                                                                                            mpc.if_else(ret_if_croi_i_diff_croi_i__1_decroi_i_e_decroi_i_1_1==-1, condition_6(i), -100), -100)))))))


        print("pourtant frérot après le result, switched vaut: ", mpc.run(mpc.output(switched)))

        def output_final_result():
            print("Solution finale : ", mpc.run(mpc.output(result)))

        print("avant le mpc if else : ", mpc.run(mpc.output(switched)))

        print(type(switched))
    
        #mpc.if_else(switched==1, output_final_result(), 0)

        return (result, switched)
        """
        print("I = : ", i)

        print(type(ret__1))
        print(ret__1)

        print(type(ret__0))
        print(ret__0)

        """

        #break

        """

        print('Maximum age:', mpc.output(ret__0))

        if (ret__0==-1):
            print("pas de solutions")
        elif (ret__1==-1):
            print("pas de solutions")
        else:
            switched = True
            print("solution : ", ret_0)

        """
        #######

        #-1 <=> pas de solutions
        #-4 <=> None
        #None
        """
        if i == max:
            print("pas de solutions !")
            break
        elif liste_decroissante[i] == liste_croissante[i]:
            print("solution (les deux échelons sont constants au meme niveau) ", i)
            break
        elif liste_decroissante[i] < liste_croissante[i]:
            #il y a eu une intersection
            switched = True
            #cas 1 : les deux courbes se sont coupées en meme temps
            if i==0:
                print("pas de solutions")
                break
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (les deux courbes se sont coupées en meme temps) : ", (liste_croissante[i-1] + liste_decroissante[i-1])/2)
            #cas 2 : la courbe de l'offre à coupée celle de la demande
            elif (liste_croissante[i] == liste_croissante[i-1]) and (liste_decroissante[i] != liste_decroissante[i-1]):
                print("solution (la courbe de l'offre à coupée celle de la demande) : ", liste_croissante[i])
            #cas 3 : la courbe de la demande à coupée celle de l'offre
            elif (liste_croissante[i] != liste_croissante[i-1]) and (liste_decroissante[i] == liste_decroissante[i-1]):
                print("solution (la courbe de la demande à coupée celle de l'offre) : ", liste_decroissante[i])
            break
        else:
            i+=1  
        """


"""
liste_croissante_0 = [1, 1, 3, 3, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
"""


#(les deux courbes se sont coupées en meme temps)

liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
liste_croissante_0 = [1, 1, 3, 4, 4, 4, 5]

#result = get_intersection(liste_croissante_0, liste_decroissante_0)

#print("result : ", result)


#(la courbe de la demande à coupée celle de l'offre)

"""
liste_decroissante_0 = [5, 4, 3, 3, 2, 2, 1]
liste_croissante_0 = [1, 1, 2, 4, 4, 4, 5]

get_intersection(liste_croissante_0, liste_decroissante_0)
"""


#(la courbe de l'offre à coupée celle de la demande)

"""
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]
liste_croissante_0 = [1, 1, 3, 3, 4, 4, 5]

get_intersection(liste_croissante_0, liste_decroissante_0)
"""


"""
liste_croissante_0 = [1, 1, 2, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 3, 3, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)

liste_croissante_0 = [1, 1, 3, 4, 4, 4, 5]
liste_decroissante_0 = [5, 4, 4, 2, 2, 2, 1]

get_intersection(liste_croissante_0, liste_decroissante_0)
"""
liste_croissante = list(map(secint, [1, 1, 3, 3, 4, 4, 5]))
liste_decroissante = list(map(secint, [5, 4, 4, 2, 2, 2, 1]))

(result, value) = get_intersection(liste_croissante, liste_decroissante)

print("result_encrypted: ", (result, value))

(result, value) = (mpc.run(mpc.output(result)), value)

print("result_decrypted: ", result, "value : ", value)

mpc.run(mpc.shutdown())